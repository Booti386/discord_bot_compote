 'use strict'
  
class StateEntryType
{
	static BIG_INT = 'i'
	static BOOLEAN = 'b'
	static NULL = 'n'
	static NUMBER = 'f'
	static OBJECT = 'o'
	static STRING = 's'
	static UNDEFINED = 'u'

	static typeOf(v)
	{
		const tmap = {
			bigint: this.BIG_INT,
			boolean: this.BOOLEAN,
			null: this.NULL,
			number: this.NUMBER,
			object: this.OBJECT,
			string: this.STRING,
			undefined: this.UNDEFINED
		}
		let t = typeof v

		if (v === null)
			t = 'null'

		if (tmap[t] === undefined)
			return null

		t = tmap[t]
		if (t === 'o' && v instanceof State)
			t += v.constructor.name

		return t
	}
}

class StateEntry
{
	t
	v

	constructor(t, v)
	{
		this.t = t
		this.v = v
	}
}

class State
{
	static classes = {}

	static register(cls)
	{
		this.classes[cls.name] = cls
	}

	static serializeObj(v)
	{
		let t = StateEntryType.typeOf(v)

		if (t === null)
			throw new Error(`Cannot serialize an expression of type '${typeof v}'`)

		if (t === StateEntryType.UNDEFINED)
			return null

		if (t[0] === StateEntryType.OBJECT)
		{
			let d = {}

			for (let i in v)
			{
				let x = this.serializeObj(v[i])
				if (x === null)
					continue

				d[i] = x
			}

			v = d
		}

		return new StateEntry(t, v)
	}

	serializeObj()
	{
		return State.serializeObj(this)
	}

	serialize()
	{
		return JSON.stringify(this.serializeObj())
	}

	static deserializeObj(e)
	{
		if (e.t === undefined
				|| e.v === undefined)
			throw new Error('Unexpected data encountered while deserializing')

		let t = e.t
		let v = e.v

		switch (t[0])
		{
			case StateEntryType.BIG_INT:
				v = BigInt(v)
				break

			case StateEntryType.BOOLEAN:
				v = Boolean(v)
				break

			case StateEntryType.NULL:
				v = null
				break

			case StateEntryType.NUMBER:
				v = Number(v)
				break

			case StateEntryType.OBJECT:
				if (t.length > 1)
				{
					t = t.substr(1)

					if (!this.classes.hasOwnProperty(t))
						throw new Error(`Class '${t}' not registered`)

					v = new (this.classes[t])()
				}

				for (let i in v)
				{
					if (e.v[i] === undefined)
						continue

					v[i] = this.deserializeObj(e.v[i])
				}
				break

			case StateEntryType.STRING:
				v = String(v)
				break

			case StateEntryType.UNDEFINED:
				v = undefined
				break

			default:
				throw new Error(`Unknown type alias '${t}'`)
		}

		return v
	}

	static deserialize(s)
	{
		return this.deserializeObj(JSON.parse(s))
	}

	clone()
	{
		return State.deserializeObj(this.serializeObj())
	}
}
