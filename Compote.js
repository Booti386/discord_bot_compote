'use strict'

function spt_(k, s, p)
{
	return k > -2 && k < 2 ? s : p
}

function spb_(k, p)
{
	return spt_(k, '', p)
}

function spbs_(k)
{
	return spb_(k, 's')
}

class CompoteState extends State
{
	enabled = false
	harvestMax = 10
	harvestRefillInc = 5
	lastActivity = 0
	harvestTotal = 0
	idPrevPlayer = null
	lastShouts = {}
	initBasket = 5
	initChest = 1
}

State.register(CompoteState)

class CompotePlayerState extends State
{
	id = null
	hasPlayed = false
	harvestCount = 0
	harvestTotal = 0
	effects = {}
	pseudo = null
	basket = null
	chest = null

	constructor(bot_state, id)
	{
		super()

		if (bot_state === undefined)
			bot_state = null
		if (id === undefined)
			id = null

		if (id !== null)
		{
			this.id = id
			this.pseudo = `<@${id}>`
		}

		if (bot_state !== null)
		{
			this.basket = bot_state.initBasket
			this.chest = bot_state.initChest
		}
	}
}

State.register(CompotePlayerState)

class CompotePlayer
{
	state
	bot

	constructor(bot, id_or_state)
	{
		if (!(id_or_state instanceof CompotePlayerState))
			id_or_state = new CompotePlayerState(bot.state, id_or_state)

		this.state = id_or_state
		this.bot = bot
	}

	isBlessed()
	{
		return this.state.effects[7777] === 7777
	}

	isCursed()
	{
		return this.state.effects[666] === 666
	}
}

class CompoteActionType
{
	static INCOME = 0 // <=> basket
	static TRANSFER = 1 // basket <=> chest
	static ROBBERY = 2 // player1.basket <=> player2.basket
	static INC_EFFECT = 3 // Increment effect
	static USE_EFFECT = 4 // Use & decrease effect
	static END_EFFECT = 5 // End marker for a list of actions added by an effect
	static ROLLBACK = 6 // The following actions are a rollback of the previous ones
	static MSG = 7 // Display a message
}

class CompoteAction
{
	type
	srcPlayer
	tgtPlayer
	amount

	constructor(type, tgt_player, amount, src_player)
	{
		if (tgt_player === undefined)
			tgt_player = null
		if (amount === undefined)
			amount = null
		if (src_player === undefined)
			src_player = null

		this.type = type
		this.srcPlayer = src_player
		this.tgtPlayer = tgt_player
		this.amount = amount
	}

	static createIncome(player, amount)
	{
		return new this(CompoteActionType.INCOME, player, amount)
	}

	static createTransfer(player, amount)
	{
		return new this(CompoteActionType.TRANSFER, player, amount)
	}

	static createRobbery(src_player, tgt_player, amount)
	{
		return new this(CompoteActionType.ROBBERY, tgt_player, amount, src_player)
	}

	static createIncEffect(player, num, amount)
	{
		return new this(CompoteActionType.INC_EFFECT, player, amount, num)
	}

	static createUseEffect(player, num, amount)
	{
		return new this(CompoteActionType.USE_EFFECT, player, amount, num)
	}

	static createEndEffect()
	{
		return new this(CompoteActionType.END_EFFECT)
	}

	static createRollback()
	{
		return new this(CompoteActionType.ROLLBACK)
	}

	static createMsg(msg)
	{
		return new this(CompoteActionType.MSG, msg)
	}
}

class CompoteEffect
{
	static TRANSFER_ON_LOSS = 5
	static REVENGE = 12
}

class CompoteTransactionFlag
{
	static ROLLBACK_ON_ALTER = 1 << 0
}

class CompoteTransaction
{
	flags
	actions

	constructor(flags)
	{
		if (flags === undefined
				|| flags === null)
			flags = 0

		this.flags = flags
		this.actions = []
	}

	addIncome(player, amount)
	{
		let act = CompoteAction.createIncome(player, amount)
		this.actions.push(act)
	}

	addTransfer(player, amount)
	{
		let act = CompoteAction.createTransfer(player, amount)
		this.actions.push(act)
	}

	addRobbery(src_player, tgt_player, amount)
	{
		let act = CompoteAction.createRobbery(src_player, tgt_player, amount)
		this.actions.push(act)
	}

	addIncEffect(player, num, amount)
	{
		let act = CompoteAction.createIncEffect(player, num, amount)
		this.actions.push(act)
	}

	addMsg(msg)
	{
		let act = CompoteAction.createMsg(msg)
		this.actions.push(act)
	}

	apply()
	{
		let pstates = {}
		let altered = false

		// Validate (1st pass)

		for (let i = 0; i < this.actions.length; i++)
		{
			let act = this.actions[i]

			let handle_transfer_on_loss = (act, is_src_else_tgt, pstate) => {
				let player = is_src_else_tgt ? act.srcPlayer : act.tgtPlayer
				let effect = pstate.effects[CompoteEffect.TRANSFER_ON_LOSS]
				let amount = is_src_else_tgt ? -act.amount : act.amount

				if (effect === undefined
						|| effect === null
						|| effect <= 0
						|| amount >= 0
						|| pstate.basket <= 0)
					return

				if (effect > pstate.basket)
					effect = pstate.basket

				let a = [
					CompoteAction.createUseEffect(player, CompoteEffect.TRANSFER_ON_LOSS, pstate.effects[CompoteEffect.TRANSFER_ON_LOSS]),
					CompoteAction.createTransfer(player, effect),
					CompoteAction.createEndEffect()
				]

				pstate.basket -= effect
				pstate.chest += effect

				this.actions.splice(i, 0, ...a)
				i += a.length

				pstate.effects[CompoteEffect.TRANSFER_ON_LOSS] = 0
				altered = true
			}

			let handle_revenge = (act, spstate, tpstate) => {
				let splayer = act.srcPlayer
				let tplayer = act.tgtPlayer
				let effect = tpstate.effects[CompoteEffect.REVENGE]
				let amount = act.amount

				if (effect === undefined
						|| effect === null
						|| effect <= 0
						|| amount >= 0)
					return

				amount = -amount * 2;

				if (amount > spstate.basket)
					amount = spstate.basket;

				let a = [
					CompoteAction.createUseEffect(tplayer, CompoteEffect.REVENGE, 1),
					CompoteAction.createRobbery(tplayer, splayer, -amount),
					CompoteAction.createEndEffect()
				]

				// This effect happens after the robbery
				this.actions.splice(i + 1, 0, ...a)
				// We have to handle the new robbery in case the other player has the effect #12 too
				i += 1

				tpstate.effects[CompoteEffect.REVENGE] -= 1
				altered = true
			}

			switch (act.type)
			{
				case CompoteActionType.INCOME:
				{
					let player = act.tgtPlayer
					let pstate = pstates[player.state.id]
					if (pstate === undefined)
						pstate = pstates[player.state.id] = player.state.clone()

					handle_transfer_on_loss(act, false, pstate)

					if (act.amount < 0)
						act.amount = -Math.min(-act.amount, pstate.basket)

					pstate.basket += act.amount
					break
				}

				case CompoteActionType.TRANSFER:
				{
					let player = act.tgtPlayer
					let pstate = pstates[player.state.id]
					if (pstate === undefined)
						pstate = pstates[player.state.id] = player.state.clone()

					if (act.amount < 0)
						throw new Error('Transfer cannot be < 0 (before validation)')

					if (act.amount > pstate.basket)
						act.amount = pstate.basket

					pstate.basket -= act.amount
					pstate.chest += act.amount
					break
				}

				case CompoteActionType.ROBBERY:
				{
					let splayer = act.srcPlayer
					let spstate = pstates[splayer.state.id]
					if (spstate === undefined)
						spstate = pstates[splayer.state.id] = splayer.state.clone()
					let tplayer = act.tgtPlayer
					let tpstate = pstates[tplayer.state.id]
					if (tpstate === undefined)
						tpstate = pstates[tplayer.state.id] = tplayer.state.clone()

					if (act.amount > 0)
					{
						handle_transfer_on_loss(act, true, spstate)

						if (act.amount > spstate.basket)
							act.amount = spstate.basket
					}
					else
					{
						handle_transfer_on_loss(act, false, tpstate)

						if (-act.amount > tpstate.basket)
							act.amount = -tpstate.basket
					}

					spstate.basket -= act.amount
					tpstate.basket += act.amount

					handle_revenge(act, spstate, tpstate)
					break
				}

				case CompoteActionType.INC_EFFECT:
				{
					let player = act.tgtPlayer
					let pstate = pstates[player.state.id]
					if (pstate === undefined)
						pstate = pstates[player.state.id] = player.state.clone()

					let num = act.srcPlayer

					if (pstate.effects[num] === undefined)
						pstate.effects[num] = 0

					pstate.effects[num] += act.amount
					break
				}

				case CompoteActionType.USE_EFFECT:
					break

				case CompoteActionType.END_EFFECT:
					break

				case CompoteActionType.ROLLBACK:
					break

				case CompoteActionType.MSG:
					break
			}
		}

		if (altered
				&& (this.flags & CompoteTransactionFlag.ROLLBACK_ON_ALTER))
		{
			let inside_effect = 0

			// Rollback everything except the effects

			let a0 = CompoteAction.createRollback()
			this.actions.push(a0)

			for (let i = this.actions.length - 1; i >= 0; i--)
			{
				let act = this.actions[i]

				switch (act.type)
				{
					case CompoteActionType.INCOME:
					{
						if (inside_effect > 0)
							break

						let a0 = CompoteAction.createIncome(act.tgtPlayer, -act.amount)
						this.actions.push(a0)
						break
					}

					case CompoteActionType.TRANSFER:
					{
						if (inside_effect > 0)
							break

						let a0 = CompoteAction.createTransfer(act.tgtPlayer, -act.amount)
						this.actions.push(a0)
						break
					}

					case CompoteActionType.ROBBERY:
					{
						if (inside_effect > 0)
							break

						let a0 = CompoteAction.createRobbery(act.srcPlayer, act.tgtPlayer, -act.amount)
						this.actions.push(a0)
						break
					}

					case CompoteActionType.INC_EFFECT:
						if (inside_effect > 0)
							break

						let a0 = CompoteAction.createIncEffect(act.tgtPlayer, act.srcPlayer, -act.amount)
						this.actions.push(a0)
						break

					case CompoteActionType.USE_EFFECT:
						// We parse the actions in reverse order
						inside_effect--
						break

					case CompoteActionType.END_EFFECT:
						// We parse the actions in reverse order
						inside_effect++
						break

					case CompoteActionType.ROLLBACK:
						break

					case CompoteActionType.MSG:
						break
				}
			}
		}

		// Apply (2nd pass)

		let msg = ''

		for (let i = 0; i < this.actions.length; i++)
		{
			let act = this.actions[i]

			switch (act.type)
			{
				case CompoteActionType.INCOME:
				{
					if (act.amount === 0)
						break

					let pstate = act.tgtPlayer.state
					let mention = `<@${pstate.id}>`

					if (act.amount > 0)
						msg += `${mention} ajoute **${act.amount}** 🍎 dans son 🧺 !\n`
					else
						msg += `${mention} perd **${-act.amount}** 🍎 de son 🧺 !\n`

					pstate.basket += act.amount
					break
				}

				case CompoteActionType.TRANSFER:
				{
					if (act.amount === 0)
						break

					let pstate = act.tgtPlayer.state
					let mention = `<@${pstate.id}>`

					if (act.amount > 0)
						msg +=`${mention} planque **${act.amount}** 🍎 dans son 🔐 !\n`
					else
						msg +=`${mention} ressort **${-act.amount}** 🍎 de son 🔐 !\n`

					pstate.basket -= act.amount
					pstate.chest += act.amount
					break
				}

				case CompoteActionType.ROBBERY:
				{
					if (act.amount === 0)
						break

					let spstate = act.srcPlayer.state
					let src_mention = `<@${spstate.id}>`
					let tpstate = act.tgtPlayer.state
					let tgt_mention = `<@${tpstate.id}>`

					if (act.amount > 0)
						msg +=`${src_mention} donne **${act.amount}** 🍎 à ${tgt_mention} !\n`
					else
						msg +=`${src_mention} vole **${-act.amount}** 🍎 à ${tgt_mention} !\n`

					spstate.basket -= act.amount
					tpstate.basket += act.amount
					break
				}

				case CompoteActionType.INC_EFFECT:
				{
					if (act.amount === 0)
						break

					let num = act.srcPlayer
					let pstate = act.tgtPlayer.state
					let mention = `<@${pstate.id}>`

					if (act.amount > 0)
						msg += `<@${pstate.id}> gagne **${act.amount}** effet${spbs_(act.amount)} **#${num}** !\n`
					else
						msg += `<@${pstate.id}> perd **${act.amount}** effet${spbs_(act.amount)} **#${num}** !\n`

					let val = pstate.effects[num]
					if (val === undefined
							|| val === null)
						pstate.effects[num] = 0

					pstate.effects[num] += act.amount
					break
				}

				case CompoteActionType.USE_EFFECT:
				{
					if (act.amount === 0)
						break

					let num = act.srcPlayer
					let pstate = act.tgtPlayer.state
					let mention = `<@${pstate.id}>`
					let rem = pstate.effects[num] - act.amount

					msg += `<@${pstate.id}> utilise **${act.amount}** effet${spbs_(act.amount)} **#${num}** ! Il lui en reste **${rem}**.\n`

					pstate.effects[num] = rem
					break
				}

				case CompoteActionType.END_EFFECT:
					break

				case CompoteActionType.ROLLBACK:
					msg += '\n**On annule tout !**\n'
					break

				case CompoteActionType.MSG:
					msg += `${act.tgtPlayer}\n`
					break
			}
		}

		return msg
	}
}

class CompoteGw extends Gw
{
	bot

	constructor(bot, token)
	{
		super(token)

		this.intents = GwIntent.GUILD
				| GwIntent.GUILD_MESSAGES
		this.bot = bot
	}

	onReady(payload)
	{
		super.onReady(payload)

		this.bot.user = payload.user

		this.updPresence(new GwPresence(GwPresenceStatus.ONLINE, false, null, new GwActivity(GwActivityType.GAME, 'Compoter')))
	}

	onMessageCreate(msg)
	{
		super.onMessageCreate(msg)

		this.bot.handleMessage(msg)
	}
}

class Compote extends Bot
{
	state
	storage
	user
	players

	constructor(token)
	{
		super(token)
		this.gw = new CompoteGw(this, token)

		this.state = new CompoteState()
		this.storage = new DBStorage('Compote://')
		this.user = null
		this.players = {}

		this.loadAll()
	}

	static genNumber(min, max)
	{
		return min + ((Math.random() * (max - min + 1)) | 0)
	}

	static genVowel()
	{
		const vowels = 'AAAAAAAAAEEEEEEEEEEEEEEEIIIIIIIIOOOOOOUUUUUUY'

		return vowels.substr((Math.random() * vowels.length) | 0, 1)
	}

	static shuffle(a)
	{
		let len = a.length

		while (len)
		{
			let idx = (Math.random() * len) | 0
			len--

			let tmp = a[idx]
			a[idx] = a[len]
			a[len] = tmp
		}

		return a
	}

	// TODO: Do it properly
	async loadAll()
	{
		this.state = null
		this.players = {}

		return Promise.all([
			this.storage.getItemOrElse('State', (new CompoteState()).serializeObj()).then(state => {
				this.state = State.deserializeObj(state)
			}),
			this.storage.getItemOrElse('Players', []).then(pstates => pstates.forEach(pstate => {
				pstate = State.deserializeObj(pstate)
				this.players[pstate.id] = new CompotePlayer(this, pstate)
			}))
		])
	}

	async saveAll()
	{
		await Promise.all([
			this.storage.setItem('State', this.state.serializeObj()),
			this.storage.setItem('Players', Object.values(this.players).map(p => p.state.serializeObj()))])
	}

	getOrCreatePlayer(id_player)
	{
		if (this.players[id_player] !== undefined)
			return this.players[id_player]

		let player = new CompotePlayer(this, id_player)
		this.players[player.state.id] = player

		this.saveAll()
		return player
	}

	getPrevPlayer()
	{
		let id_prev_player = this.state.idPrevPlayer
		if (id_prev_player === undefined
				|| id_prev_player === null)
			return null

		return this.getOrCreatePlayer(id_prev_player)
	}

	getPlayersRanked()
	{
		let sorted = Object.values(this.players)
			.filter(p => p.state.hasPlayed)
			.sort((p1, p2) => {
				// Sort in reverse order
				let diff = (p2.state.basket + p2.state.chest) - (p1.state.basket + p1.state.chest)
				if (diff !== 0)
					return diff

				return p2.state.chest - p1.state.chest
			})
		let ranked = []
		let prev_p = null

		sorted.forEach(p => {
			if (prev_p !== null
					&& p.state.basket === prev_p.state.basket
					&& p.state.chest === prev_p.state.chest)
			{
				ranked[ranked.length - 1].push(p)
				return
			}

			ranked[ranked.length] = [ p ]
			prev_p = p
		})

		return ranked.map(ps => Compote.shuffle(ps))
	}

	getPlayerRank(player)
	{
		let players = this.getPlayersRanked()

		let rank = players.findIndex(ps => ps.some(p => p.state.id === player.state.id))
		return rank < 0 ? null : rank
	}

	getPlayersRankedAbove(player)
	{
		let players = this.getPlayersRanked()
		let player_rank = players.findIndex(ps => ps.some(p => p.state.id === player.state.id))

		return players.filter((ps, rank) => rank < player_rank)
	}

	getFirstRankedPlayers()
	{
		let players = this.getPlayersRanked()

		if (players.length > 0)
			return players[0]
		return []
	}

	getPlayersRankedJustBelow(player)
	{
		let players = this.getPlayersRanked()
		let player_rank = players.findIndex(ps => ps.some(p => p.state.id === player.state.id))

		if (player_rank < players.length - 1)
			return players[player_rank + 1]
		return []
	}

	async handleMessage(msg)
	{
		if (msg.content === undefined
				|| msg.content === null)
			return

		let player = this.getOrCreatePlayer(msg.author.id, msg.author)
		let pstate = player.state

		pstate.pseudo = msg.author.username
		if (msg.member.nick !== undefined
				&& msg.member.nick !== null)
			pstate.pseudo = msg.member.nick

		if (msg.content.match(`^\\s*<@!?${this.user.id}>\\s*$`) === null)
		{
			this.handleCommand(msg)
			return
		}

		if (!this.state.enabled)
			return

		let pharvest_total_before = pstate.harvestTotal

		let ans = this.processRefill()
		ans += this.processHarvest(player)

		let dummy_harvest = false
		if (pharvest_total_before === pstate.harvestTotal)
			dummy_harvest = true

		// The increment must be here because it is used as a global ID to sort the messages
		this.state.harvestTotal++

		// Cache before async processing
		let pharvest_total = pstate.harvestTotal
		let harvest_total = this.state.harvestTotal

		await this.saveAll()

		this.rest.createEmbedMessage(msg.channel_id,
				`Cueillette ${dummy_harvest ? 'ratée' : `#${pharvest_total}`} de ${pstate.pseudo}`,
				ans,
				null,
				`#${harvest_total}`)
	}

	handleCommandSetPlayer(args)
	{
		if (args.length < 3)
			return null

		let result = args[1].match(/^<@!?([0-9]+)>$/)
		if (result === null)
			return null

		let player = this.getOrCreatePlayer(result[1])
		let pstate = player.state

		let has_played = null
		let harvest_count = null
		let basket = null
		let chest = null
		let effects = null

		switch (args[2])
		{
			case 'has_played':
				if (args.length != 4)
					return null

				has_played = (args[3] | 0) !== 0
				break

			case 'harvest':
				if (args.length != 4)
					return null

				harvest_count = args[3] | 0
				break

			case 'basket':
				if (args.length != 4)
					return null

				basket = args[3] | 0
				break

			case 'chest':
				if (args.length != 4)
					return null

				chest = args[3] | 0
				break

			case 'effects':
			{
				if (args.length < 4)
					return null
				
				let arg_effects = args.slice(3)
				if (arg_effects.length % 2 !== 0)
					return null

				effects = []
				for (let i = 0; i < arg_effects.length; i += 2)
					effects[arg_effects[i] | 0] = arg_effects[i + 1] | 0
				break
			}

			case 'all':
			{
				if (args.length < 4)
					return null

				harvest_count = args[3] | 0

				if (args.length < 5)
					break
				basket = args[4] | 0

				if (args.length < 6)
					break
				chest = args[5] | 0

				if (args.length < 7)
					break

				let arg_effects = args.slice(6)
				if (arg_effects.length % 2 !== 0)
					return null

				effects = []
				for (let i = 0; i < arg_effects.length; i += 2)
					effects[arg_effects[i] | 0] = arg_effects[i + 1] | 0
				break
			}

			case 'reset':
				if (args.length != 3)
					return null

				this.resetPlayer(player)
				break

			default:
				return null
		}

		if (has_played !== null)
			pstate.hasPlayed = has_played

		if (harvest_count !== null)
			pstate.harvestCount = harvest_count

		if (basket !== null)
			pstate.basket = basket

		if (chest !== null)
			pstate.chest = chest

		if (effects !== null)
		{
			effects.forEach((effect, id) => {
				pstate.effects[id] = effect
			})
		}

		return this.genPlayerSummary(player)
	}

	handleCommandSetLastShout(args)
	{
		if (args.length != 4)
			return null

		if (args[1] !== 'last_shout')
			return null

		if (args[2].match(/^A|E|I|O|U|Y$/i) === null)
			return null

		let result = args[3].match(/^<@!?([0-9]+)>$/)
		if (result === null)
			return null

		this.state.lastShouts[args[2]] = result[1]

		return `Le dernier joueur à avoir hurlé **${args[2]}** est désormais <@${this.state.lastShouts[args[2]]}>`
	}

	handleCommandSetLastPlayer(args)
	{
		if (args.length != 3)
			return null

		if (args[1] !== 'last_player')
			return null

		let result = args[2].match(/^<@!?([0-9]+)>$/)
		if (result === null)
			return null

		this.state.idPrevPlayer = result[1]

		return `Le dernier joueur à avoir joué est désormais <@${this.state.idPrevPlayer}>`
	}

	handleCommandSetHarvestMax(args)
	{
		if (args.length != 3)
			return null

		if (args[1] !== 'harvest_max')
			return null

		this.state.harvestMax = args[2] | 0

		return `Il y a désormais ${this.state.harvestMax} cueillette${spbs_(this.state.harvestMax)} journalière${spbs_(this.state.harvestMax)}`
	}

	async handleCommandSet(msg, args)
	{
		if (args[0] !== 'set')
			return false

		if (!this.isAdmin(msg.author.id))
		{
			this.rest.createEmbedMessage(msg.channel_id, 'Erreur', 'Commande non autorisée')
			return true
		}

		let result = this.handleCommandSetPlayer(args)
		if (result === null)
			result = this.handleCommandSetLastShout(args)
		if (result === null)
			result = this.handleCommandSetLastPlayer(args)
		if (result === null)
			result = this.handleCommandSetHarvestMax(args)
		if (result === null)
			return false

		await this.saveAll()

		this.rest.createEmbedMessage(msg.channel_id,
				'Résultat',
				result)
		return true
	}

	async handleCommandEnable(msg, args)
	{
		if (args.length !== 1
				|| args[0] !== 'enable')
			return false

		if (!this.isAdmin(msg.author.id))
		{
			this.rest.createEmbedMessage(msg.channel_id, 'Erreur', 'Commande non autorisée')
			return true
		}

		this.state.enabled = true

		await this.saveAll()

		this.rest.createEmbedMessage(msg.channel_id,
				'Résultat',
				`<@${this.user.id}> activé !`)
		return true
	}

	async handleCommandDisable(msg, args)
	{
		if (args.length !== 1
				|| args[0] !== 'disable')
			return false

		if (!this.isAdmin(msg.author.id))
		{
			this.rest.createEmbedMessage(msg.channel_id, 'Erreur', 'Commande non autorisée')
			return true
		}

		this.state.enabled = false

		await this.saveAll()

		this.rest.createEmbedMessage(msg.channel_id,
				'Résultat',
				`<@${this.user.id}> désactivé !`)
		return true
	}

	async handleCommandShow(msg, args)
	{
		if (args.length < 1
				|| args.length > 2)
			return false

		if (args[0] !== 'show'
				&& args[0] !== 'info')
			return false

		if (args.length === 1)
			args[1] = `<@${msg.author.id}>`

		let result = args[1].match(/^<@!?([0-9]+)>$/)
		if (result === null)
			return false

		let player = this.getOrCreatePlayer(result[1])

		this.rest.createEmbedMessage(msg.channel_id,
				player.pseudo,
				this.genPlayerSummary(player))
		return true
	}

	async handleCommandRules(msg, args)
	{
		if (args.length !== 1
				|| args[0] !== 'rules')
			return false

		this.rest.createEmbedMessage(msg.channel_id,
				'Règles',
				Compote.getRules())
		return true
	}

	async handleCommandReset(msg, args)
	{
		let reset_str = 'jesouhaitetuertouteslespommes'

		if (args.length < 1
				|| args[0] !== 'reset')
			return false

		if (!this.isAdmin(msg.author.id))
		{
			this.rest.createEmbedMessage(msg.channel_id, 'Erreur', 'Commande non autorisée')
			return true
		}

		if (args.length === 1)
		{
			this.rest.createEmbedMessage(msg.channel_id,
					'Attention',
					'La réinitialisation supprimera **toutes les données** de façon irréversible !\n'
					+ 'Pour valider, effectuer la commande suivante :\n'
					+ `<@${this.user.id}> reset ${reset_str}`)
			return true
		}

		if (args.length !== 2)
			return false

		if (args[1] !== reset_str)
			return false

		await this.storage.clear()
		await this.loadAll()

		this.rest.createEmbedMessage(msg.channel_id,
				'Réinitialisation',
				'Réinitialisation complète effectuée')
		return true
	}

	static rankToMedal(rank)
	{
		const medals = [ '🎖️', '🥇', '🥈', '🥉', '🏅' ]
		return medals[Math.min(Math.abs(rank), medals.length - 1)]
	}

	async handleCommandRank(msg, args)
	{
		if (args.length !== 1
				|| args[0] !== 'rank')
			return false

		let players = ''
		let apples = ''

		this.getPlayersRanked().forEach((ps, idx) => ps.forEach(p => {
			let pstate = p.state
			let rank = idx + 1

			let medal = Compote.rankToMedal(rank)
			let prefix = `${medal} **${rank}.** `
			let suffix = ''

			if (p.isBlessed())
				suffix += ' 👼'
			if (p.isCursed())
				suffix += ' 😈'

			players += `${prefix}<@${pstate.id}>${suffix}\n`
			apples += `**${pstate.basket + pstate.chest}** 🍎 *(🔐 ${pstate.chest} - 🧺 ${pstate.basket})*\n`
		}))

		if (players === '')
			players = 'Personne...'

		if (apples === '')
			apples = '🍐'

		this.rest.createEmbedMessage(msg.channel_id,
			'🏆 Classement',
			'',
			[{
				name: 'Participants',
				value: players,
				inline: true
			}, {
				name: 'Pommes',
				value: apples,
				inline: true
			}])

		return true
	}

	async handleCommandHarvest(msg, args)
	{
		if (args.length < 2
				|| args.length > 5)
			return false

		if (args[0] !== 'harvest')
			return false

		if (!this.isAdmin(msg.author.id))
		{
			this.rest.createEmbedMessage(msg.channel_id, 'Erreur', 'Commande non autorisée')
			return true
		}

		let result = args[1].match(/^<@!?([0-9]+)>$/)
		if (result === null)
			return false
		let player = this.getOrCreatePlayer(result[1])
		let pstate = player.state

		let num = args[2] ?? null
		let vowel = args[3] ?? null
		let force = args[4] ?? null

		if (num !== null)
			num |= 0
		if (force !== null)
			force |= 0

		let ans = this.processHarvest(player, num, vowel, force)

		this.state.harvestTotal++

		// Cache before async processing
		let pharvest_total = pstate.harvestTotal
		let harvest_total = this.state.harvestTotal

		await this.saveAll()

		this.rest.createEmbedMessage(msg.channel_id,
				`${pstate.pseudo} #${pharvest_total}`,
				ans,
				null,
				`#${harvest_total}`)
		return true
	}

	async handleCommand(msg)
	{
		if (msg.content.match(`^\\s*<@!?${this.user.id}>\\s+`) === null)
			return

		let args = msg.content.trim().replace(/\s+/g, ' ').split(' ')

		args.shift()

		let result

		result = await this.handleCommandSet(msg, args)
		if (!result)
			result = await this.handleCommandEnable(msg, args)
		if (!result)
			result = await this.handleCommandDisable(msg, args)
		if (!result)
			result = await this.handleCommandShow(msg, args)
		if (!result)
			result = await this.handleCommandRules(msg, args)
		if (!result)
			result = await this.handleCommandReset(msg, args)
		if (!result)
			result = await this.handleCommandRank(msg, args)
		if (!result)
			result = await this.handleCommandHarvest(msg, args)
		if (!result)
			this.rest.createEmbedMessage(msg.channel_id,
					'Erreur',
					'Commande inconnue')
	}

	static amountOfDailySplitsBetween(low, high, splits)
	{
		let count = 0
		let low_up = (new Date(low)).setHours(24, 0, 0, 0)
		let high_down = (new Date(high)).setHours(0, 0, 0, 0)

		for (let s of splits)
		{
			let tmpl = (new Date(low)).setHours(...s)
			let tmph = (new Date(high)).setHours(...s)

			// Both are on the same day
			if (low_up > high_down)
			{
				if (tmpl >= low && tmph <= high)
					count++
				continue
			}

			if (tmpl >= low)
				count++
			if (tmph <= high)
				count++
		}

		if (low_up < high_down)
			count += splits.length * ((high_down - low_up) / (1000 * 3600 * 24) | 0)

		return count
	}

	processRefill()
	{
		let last_activity = this.state.lastActivity
		let now = new Date()

		this.state.lastActivity = now.getTime()

		let count = Compote.amountOfDailySplitsBetween(last_activity, now, [
			[0,  0, 0, 0],
			[12, 0, 0, 0]
		])

		if (count === 0)
			return ''

		let amount = Math.min(this.state.harvestMax, count * this.state.harvestRefillInc)

		for (let i in this.players)
		{
			let pstate = this.players[i].state

			pstate.harvestCount = Math.max(0, pstate.harvestCount - amount)
		}

		return '*Des trognons se décomposent, des fleurs éclosent, des pommes mûrissent... C\'est le cycle de la vie.*\n'
				+ `Chaque joueur obtient **${amount}**/${this.state.harvestMax} cueillette${spbs_(amount)} supplémentaire${spbs_(amount)} !\n\n`
	}

	genTransaction(player, num, vowel)
	{
		let state = this.state
		let pstate = player.state
		let tr = new CompoteTransaction()

		switch (num)
		{
			case 1:
			{
				let amount = pstate.effects[num]
				if (amount === undefined
						|| amount === null)
					amount = 0
				amount++

				tr.addIncome(player, amount)
				tr.addIncEffect(player, num, 1)
				break
			}

			case 2:
				if (pstate.basket > pstate.chest)
					tr.addTransfer(player, 5)
				else
					tr.addIncome(player, 5)
				break

			case 3:
				if (pstate.basket > pstate.chest)
					tr.addTransfer(player, 3)
				else
					tr.addIncome(player, 3)
				break

			case 4:
				if (pstate.basket > pstate.chest)
					tr.addTransfer(player, 1)
				else
					tr.addIncome(player, 1)
				break

			case 5:
			{
				let shout = 'OU'.includes(vowel)
				let amount = shout ? 3 : 1

				tr.addIncEffect(player, num, amount)
				break
			}

			case 6:
				tr.addIncome(player, 4)
				break

			case 7:
				tr.addIncome(player, 2)
				break

			case 8:
			{
				let shout = 'Y'.includes(vowel)
				let amount = shout ? -2 : -1

				tr.addIncome(player, amount)
				break
			}

			case 9:
			{
				let shout = 'Y'.includes(vowel)
				let amount = shout ? -6 : -3

				tr.addIncome(player, amount)
				break
			}

			case 10:
			{
				let amount = Math.max(0, pstate.basket - pstate.chest)

				tr.addIncome(player, -amount)
				break
			}

			case 11:
				break

			case 12:
				tr.addIncEffect(player, num, 1)
				break

			case 13:
			{
				let id_last_shout_player = state.lastShouts[vowel]
				if (id_last_shout_player === undefined
						|| id_last_shout_player === null)
					break

				let last_shout_player = this.getOrCreatePlayer(id_last_shout_player)

				tr.addRobbery(player, last_shout_player, -1)
				break
			}

			case 14:
			{
				let prev_player = this.getPrevPlayer()
				if (prev_player === null)
					break

				tr.addRobbery(player, prev_player, -2)
				break
			}

			case 15:
			{
				let prev_player = this.getPrevPlayer()
				if (prev_player === null)
					break

				let shout = 'AEI'.includes(vowel)
				let amount = shout ? -3 : 3

				tr.addRobbery(player, prev_player, amount)
				break
			}

			case 16:
			{
				let prev_player = this.getPrevPlayer()
				if (prev_player === null)
					break

				let ppstate = prev_player.state

				if (pstate.basket + pstate.chest >= ppstate.basket + ppstate.chest)
					break

				let shout = 'OU'.includes(vowel)
				if (!shout)
					break

				let amount = Math.ceil(ppstate.basket / 2)

				tr.addRobbery(player, prev_player, -amount)
				break
			}

			case 17:
			{
				let shout = 'OU'.includes(vowel)
				let prev_player = this.getPrevPlayer()

				if (prev_player !== null)
					tr.addIncome(prev_player, -1)
				if (shout)
					tr.addIncome(player, -1)
				break
			}

			case 18:
				this.getPlayersRankedAbove(player).flat().forEach(p => {
					tr.addRobbery(player, p, -1)
				})
				break

			case 19:
			{
				let shout = 'A'.includes(vowel)
				let amount = shout ? -2 : -1

				this.getFirstRankedPlayers().forEach(p => {
					tr.addIncome(p, amount)
				})
				break
			}

			case 20:
			{
				let shout = 'Y'.includes(vowel)
				let amount = state.harvestMax - (pstate.harvestCount + 1)

				if (amount <= 0)
				{
					let amount = shout ? 1 : 2

					tr.addMsg(`Vous obtenez ${amount} cueillette${spbs_(amount)} supplémentaire${spbs_(amount)} !`)
					pstate.harvestCount -= amount
					break
				}

				tr.addIncome(player, -amount)
				break
			}

			default:
				break
		}

		return tr
	}

	static getActionDesc(num)
	{
		const acts = [
			'Vous ajoutez à votre panier autant de pommes que le nombre de fois où vous avez fait 1.',
			'Planquez 5 pommes de votre panier s\'il est plus gros que votre coffre, sinon ajoutez-les à votre panier.',
			'Planquez 3 pommes de votre panier s\'il est plus gros que votre coffre, sinon ajoutez-les à votre panier.',
			'Planquez 1 pomme de votre panier s\'il est plus gros que votre coffre, sinon ajoutez-la à votre panier.',
			'Avant votre prochaine perte de pommes, vous en planquerez une dans votre coffre, trois si vous hurlez O ou U.',
			'Ajoutez 4 pommes à votre panier.',
			'Ajoutez 2 pommes à votre panier.',
			'Votre panier perd une pomme, le double si vous hurlez Y.',
			'Votre panier perd trois pommes, le double si vous hurlez Y.',
			'Vous perdez des pommes de votre panier pour en avoir autant que dans votre coffre.',
			'Il ne se passe rien.',
			'La prochaine fois qu\'un joueur vous volera des pommes, vous riposterez en lui volant le double.',
			'Vous volez une pomme au panier du précédent joueur ayant hurlé comme vous.',
			'Vous volez deux pommes au panier du joueur ayant joué avant vous.',
			'Si vous hurlez A, E ou I, vous volez trois pommes au panier du joueur précédent, sinon vous lui en donnez trois.',
			'Si vous avez moins de pommes que le joueur précédent et que vous hurlez O ou U, vous lui volez la moitié des pommes de son panier.',
			'Le joueur précédent perd une pomme de son panier, vous aussi si vous hurlez O ou U.',
			'Vous volez une pomme à chaque personne au-dessus de vous au classement.',
			'Le premier (ou les premiers si ex-aequo) du classement perd une pomme de son panier, deux si vous avez hurlé A.',
			'Votre panier perd autant de pommes que de lancers qu\'il vous reste après celui-ci. Si vous n\'en avez plus, vous gagnez deux lancers, un seul si vous hurlez Y.'
		]

		if (num < 0)
			return acts

		return acts[num - 1]
	}

	static getRules()
	{
		return this.getActionDesc(-1).reduce((acc, val, i) => acc + `${i + 1}) ${val}\n`, '')
	}

	processHarvest(player, num, vowel, force)
	{
		let state = this.state
		let pstate = player.state

		num ??= Compote.genNumber(1, 20)
		vowel ??= Compote.genVowel()
		force ??= false

		if (!force)
		{
			if (pstate.harvestCount >= state.harvestMax)
				return `Tu as déjà cueilli suffisamment de 🍎, <@${pstate.id}> ! Laisse les autres en profiter !`

			if (state.idPrevPlayer === pstate.id)
				return `Tu es la dernière personne à avoir joué, <@${pstate.id}> ! Attends encore une personne, et tu pourras rejouer.`
		}

		pstate.hasPlayed = true

		let msg = `<@${pstate.id}> obtient **${num}** et hurle **${vowel}** !\n`
		let desc = Compote.getActionDesc(num)
		msg += `*${desc}*\n\n`

		let tr = this.genTransaction(player, num, vowel)
		let msg_tr = tr.apply()

		let blessed = player.isBlessed()
		let cursed = player.isCursed()

		if (Math.random() <= 0.02
				&& (cursed || blessed))
		{
			let tr = new CompoteTransaction()

			if (cursed && blessed)
			{
				blessed = Math.random() <= 0.50
				cursed = !blessed
			}

			if (blessed)
			{
				tr.addMsg('\n**Bénédiction !**')
				tr.addIncome(player, 7777)
			}

			if (cursed)
			{
				tr.addMsg('\n**Malédiction !**')
				tr.addIncome(player, -666)
			}

			tr.addMsg('\nOn dirait que quelqu\'un a cassé le bot...')
			this.state.enabled = false

			msg_tr += tr.apply()
		}

		if (msg_tr === '')
			msg_tr = `Pas de chance pour <@${pstate.id}> ! Rien cette fois !\n`

		msg += msg_tr

		state.idPrevPlayer = pstate.id
		state.lastShouts[vowel] = pstate.id
		pstate.harvestCount++
		pstate.harvestTotal++

		let summary = this.genPlayerSummary(player)

		msg += `\n${summary}`

		return msg
	}

	genPlayerSummary(player)
	{
		let pstate = player.state
		let rem_harvest = Math.max(0, this.state.harvestMax - pstate.harvestCount)

		let msg = `Tu possèdes **${pstate.basket + pstate.chest}** 🍎 ! *(🔐 ${pstate.chest} - 🧺 ${pstate.basket})*\n`
		msg += `Il te reste **${rem_harvest}**/${this.state.harvestMax} cueillette${spbs_(rem_harvest)}.`

		if (player.isBlessed())
			msg += '\n*Tu es **béni** ! Quelle chance !*'

		if (player.isCursed())
			msg += '\n*Tu es **maudit**. Bonne chance.*'

		let msg_effects = Object.entries(pstate.effects).map((pair) => {
			let id = pair[0]
			let val = pair[1]

			if (val <= 0)
				return null

			return `**#${id} x${val}**`
		}).filter(s => s !== null).join(', ')

		if (msg_effects !== '')
			msg += `\nEffets : ${msg_effects}`

		let rank = this.getPlayerRank(player)
		if (rank !== null)
		{
			rank++
			let medal = Compote.rankToMedal(rank)
			msg += `\nRang : ${medal} **${rank}**`
		}

		return msg
	}

	// TODO
	isAdmin(id_player)
	{
		return false
	}

	resetPlayer(player)
	{
		this.players[player.state.id] = new Player(this, player.state.id)
		this.saveAll()
	}
}
