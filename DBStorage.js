'use strict'

class DBStorage
{
	name
	db

	constructor(name)
	{
		this.name = name
		this.db = new Promise((resolve, reject) => {
			let rq = window.indexedDB.open(this.name, 1)
			rq.onerror = ev => {
				reject(ev)
			}

			rq.onsuccess = ev => {
				let db = rq.result
				resolve(db)
			}

			rq.onupgradeneeded = ev => {
				let db = rq.result
				db.createObjectStore('root', { autoIncrement: true })
				return true
			}
		})
	}

	static emitRequest(instance, rq_name, need_result)
	{
		return instance.db.then(db => {
			let args = Array.prototype.slice.call(arguments, 3)

			let transaction = db.transaction('root', 'readwrite')
			let store = transaction.objectStore('root')
			let rq = store[rq_name].apply(store, args)

			return new Promise((resolve, reject) => {
				transaction.onerror = ev => {
					reject(ev)
				}

				rq.onerror = ev => {
					reject(ev)
				}

				rq.onsuccess = ev => {
					if (!need_result)
						return resolve()

					if (typeof rq.result === 'undefined')
						return rq.onerror(ev)

					resolve(rq.result)
				}
			})
		}, err => {
			return Promise.reject()
		})
	}

	key(idx)
	{
		throw new Error('Not implemented')
	}

	getItem(key_name)
	{
		return DBStorage.emitRequest(this, 'get', true, key_name)
	}

	getItemOrElse(key_name, or_else)
	{
		return this.getItem(key_name).catch(() => or_else)
	}

	setItem(key_name, value)
	{
		return DBStorage.emitRequest(this, 'put', true, value, key_name)
	}

	removeItem(key_name)
	{
		return DBStorage.emitRequest(this, 'delete', false, key_name)
	}

	clear()
	{
		return DBStorage.emitRequest(this, 'clear', false)
	}

	getItems(key_names)
	{
		return Promise.all(key_names.map(key_name => this.getItem(key_name)))
	}

	getItemsOrElse(key_names, or_else)
	{
		return Promise.all(key_names.map((key_name, idx) => this.getItemOrElse(key_name, or_else[idx])))
	}

	setItems(key_names, key_vals)
	{
		return Promise.all(key_names.map((key_name, idx) => this.setItem(key_name, key_vals[idx])))
	}
}
