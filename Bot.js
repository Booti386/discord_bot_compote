'use strict'

// Join URL: https://discord.com/api/oauth2/authorize?response_type=code&client_id=xxxxx&scope=bot&redirect_uri=https://myuri.com/

if (!console)
	console = {}
if (!console.log)
	console.log = () => {}

class Enum
{
	static asString(val)
	{
		for (let k in this)
		{
			if (this[k] === val)
				return `${k} (${val})`
		}

		return `<unknown> (${val})`
	}
}

class Bitfield
{
	static asString(val)
	{
		let r = ''
		let v = val

		for (let k in this)
		{
			let flag = this[k]

			if (flag & v
					|| (val === 0 && flag === 0))
			{
				if (r !== '')
					r += ' | '
				r += `${k} (${flag})`

				v &= ~flag
			}
		}

		if (v != 0 || r === '')
		{
			if (r !== '')
				r += ' | '
			r += `<unknown> (${v})`
		}

		return r
	}
}

class Util
{
	static byteToHex(b)
	{
		return (b + 0x100).toString(16).substr(1).toLowerCase()
	}

	static genSessId()
	{
		return Math.random().toString().substr(2, 21)
	}

	static genRandBytes(count)
	{
		let buf = new Uint8Array(count)

		window.crypto.getRandomValues(buf)

		return buf
	}

	static genUuidv4()
	{
		let buf = this.genRandBytes(16)

		buf[6] = (buf[6] & 0x0f) | 0x40
		buf[8] = (buf[8] & 0x3f) | 0x80

		return buf
	}

	static uuidToStr(uuid)
	{
		let str = ''

		for (let b in uuid)
		{
			str += this.byteToHex(b)

			if (b === 3 || b === 5 || b === 7 || b === 9)
				str += '-'
		}

		return str
	}
}

class WsCloseEventCode extends Enum
{
	static NORMAL_CLOSURE = 1000
	static GOING_AWAY = 1001
	static PROTOCOL_ERROR = 1002
	static UNSUPPORTED_DATA = 1003
	static NO_STATUS_RECEIVED = 1005
	static ABNORMAL_CLOSURE = 1006
	/* ... */
}

class Ws
{
	ws
	url
	autoReconnectTimeout

	constructor(url, auto_reconnect_timeout)
	{
		if (url === undefined)
			url = null
		if (auto_reconnect_timeout === undefined
				|| auto_reconnect_timeout === null)
			auto_reconnect_timeout = 3

		this.ws = null
		this.url = url
		this.autoReconnectTimeout = auto_reconnect_timeout
	}

	boot(url)
	{
		if (url === undefined)
			url = this.url
		this.url = url

		this.connect()
	}

	connect()
	{
		this.ws = new WebSocket(this.url)

		this.ws.onopen = ev => this.onWsOpen(ev)
		this.ws.onmessage = ev => this.onWsMsg(ev)
		this.ws.onerror = ev => this.onWsError(ev)
		this.ws.onclose = ev => this.onWsClose(ev)
	}

	onWsOpen(ev)
	{
		console.log('Ws.onWsOpen()')
	}

	onWsMsg(ev)
	{
		console.log('Ws.onWsMsg(): ', ev.data)
	}

	onWsError(ev)
	{
		console.log('Ws.onWsError(): ', ev)
	}

	onWsClose(ev)
	{
		let close_msg = WsCloseEventCode.asString(ev.code)

		let reason = ev.reason
		if (reason !== '')
			reason = `: ${reason}`

		console.log(`Ws.onWsClose(): ${close_msg}${reason}`)
		this.ws = null

		if (this.autoReconnectTimeout > 0)
		{
			window.setTimeout(() => {
				console.log(`Ws.onWsClose(): Reconnecting in ${this.autoReconnectTimeout} seconds...`)
				window.setTimeout(() => this.connect(), this.autoReconnectTimeout * 1000)
			}, 0)
		}
	}
}

class GwOp extends Enum
{
	static DISPATCH = 0
	static HEARTBEAT = 1
	static IDENT = 2
	static UPD_STATUS = 3
	static UPD_VOICE_STATE = 4
	static RESUME = 6
	static RECONNECT = 7
	static REQUEST_GUILD_MEMBERS = 8
	static INVAL_SESSION = 9
	static HELLO = 10
	static HEARTBEAT_ACK = 11
}

class GwCloseEventCode extends WsCloseEventCode
{
	static UNK_ERROR = 4000
	static UNK_OPCODE = 4001
	static DECODE_ERR = 4002
	static NOT_AUTH = 4003
	static AUTH_FAILED = 4004
	static ALREADY_AUTH = 4005
	static INVAL_SEQ = 4007
	static RATE_LIMITED = 4008
	static SESSION_TIMEOUT = 4009
	static INVAL_SHARD = 4010
	static SHARD_REQ = 4011
	static INVAL_API_VER = 4012
	static INVAL_INTENT = 4013
	static DISALLOWED_INTENT = 4014
}

class GwIntent extends Bitfield
{
	static NONE = 0
	static GUILD = 1 << 0
	static GUILD_MEMBERS = 1 << 1
	static GUILD_BANS = 1 << 2
	static GUILD_EMOJIS = 1 << 3
	static GUILD_INTEGRATIONS = 1 << 4
	static GUILD_WEBHOOKS = 1 << 5
	static GUILD_INVITES = 1 << 6
	static GUILD_VOICE_STATES = 1 << 7
	static GUILD_PRESENCES = 1 << 8
	static GUILD_MESSAGES = 1 << 9
	static GUILD_MESSAGE_REACTIONS = 1 << 10
	static GUILD_MESSAGE_TYPING = 1 << 11
	static DIRECT_MESSAGES = 1 << 12
	static DIRECT_MESSAGE_REACTIONS = 1 << 13
	static DIRECT_MESSAGE_TYPING = 1 << 14
}

class GwPresenceStatus extends Enum
{
	static ONLINE = 'online'
	static DND = 'dnd'
	static IDLE = 'idle'
	static INVISIBLE = 'invisible'
	static OFFLINE = 'offline'
}

class GwActivityType extends Enum
{
	static GAME = 0
	static STREAMING = 1
	static LISTENING = 2
	static CUSTOM = 3
}

class GwActivity
{
	type       // GwActivityType
	name       // String
	created_at // unix_ts
	/* ... */

	constructor(type, name, created_at)
	{
		let now = Date.now()

		if (type === undefined)
			type = GwActivityType.GAME
		if (name === undefined)
			name = ''
		if (created_at === undefined)
			created_at = now

		this.type = type
		this.name = name
		this.created_at = created_at
	}
}

class GwPresence
{
	status // GwPresenceStatus
	afk    // bool
	since  // unix_ts
	game   // GwActivity

	constructor(status, afk, since, game)
	{
		if (status === undefined)
			status = GwPresenceStatus.ONLINE
		if (afk === undefined)
			afk = false
		if (since === undefined)
			since = null
		if (game === undefined)
			game = null

		this.status = status
		this.afk = afk
		this.since = since
		this.game = game
	}
}

class Gw extends Ws
{
	static API_VER = 6
	static API_ROOT = 'https://discord.com/api'

	lastSeq
	isUser
	idSession
	heartbeatInterval
	heartbeatIntervalId
	token
	identProps
	intents
	presence

	constructor(token, auto_reconnect_timeout)
	{
		super(null, auto_reconnect_timeout)

		this.lastSeq = null
		this.idUser = null
		this.idSession = null
		this.heartbeatInterval = null
		this.heartbeatIntervalId = null
		this.token = token
		this.identProps = {
			os: '',
			browser: '',
			device: '',
			browser_user_agent: '',
			browser_version: '',
			os_version: '',
			referrer: '',
			referring_domain: '',
			referrer_current: '',
			referring_domain_current: ''
		}
		this.intents = GwIntent.GUILD
		this.presence = new GwPresence()
	}

	boot()
	{
		let rq = fetch(`${Gw.API_ROOT}/gateway`)
		rq.then(ans => ans.json())
				.then(data => super.boot(`${data.url}?v=${Gw.API_VER}&encoding=json`))
	}

	updPresence(presence)
	{
		this.presence = presence
		this.sendUpdStatus()
	}

	send(op, data)
	{
		let op_msg = GwOp.asString(op)
		let d = {
			op: op
		}

		if (data !== undefined)
			d.d = data

		d = JSON.stringify(d)

		console.log(`Gw.send(op = ${op_msg}): ${d}`)

		this.ws.send(d)
	}

	sendIdent()
	{
		return this.send(GwOp.IDENT, {
			token: this.token,
			intents: this.intents,
			properties: this.identProps,
			presence: this.presence
		})
	}

	sendUpdStatus()
	{
		return this.send(GwOp.UPD_STATUS, this.presence)
	}

	sendUpdVoiceState(id_guild, id_channel, self_mute, self_deaf)
	{
		if (id_channel === undefined)
			id_channel = null
		if (self_mute === undefined)
			self_mute = false
		if (self_deaf === undefined)
			self_deaf = false

		return this.send(GwOp.UPD_VOICE_STATE, {
			guild_id: id_guild,
			channel_id: id_channel,
			self_mute: self_mute,
			self_deaf: self_deaf
		})
	}

	sendResume()
	{
		return this.send(GwOp.RESUME, {
			token: this.token,
			session_id: this.idSession,
			seq: this.lastSeq
		})
	}

	sendHeartbeat()
	{
		return this.send(GwOp.HEARTBEAT, this.lastSeq)
	}

	onWsMsg(ev)
	{
		super.onWsMsg(ev)

		let data

		try
		{
			data = JSON.parse(ev.data)
		}
		catch (ex)
		{
			throw new Error(`Invalid message: ${ev.data}`)
		}

		let op_msg = GwOp.asString(data.op)

		console.log(`Gw.onWsMsg(): op = ${op_msg}`)

		switch (data.op)
		{
			case GwOp.DISPATCH: this.onMsgDispatch(data); break
			case GwOp.RECONNECT: this.onMsgReconnect(data); break
			case GwOp.INVAL_SESSION: this.onMsgInvalSession(data); break
			case GwOp.HELLO: this.onMsgHello(data); break
			case GwOp.HEARTBEAT_ACK: this.onMsgHeartbeatAck(data); break
			default:
				console.log(`Gw.onWsMsg(): Message not handled`)
				break
		}
	}

	onWsClose(ev)
	{
		super.onWsClose(ev)

		let close_msg = GwCloseEventCode.asString(ev.code)

		let reason = ev.reason
		if (reason !== '')
			reason = `: ${reason}`

		console.log(`Gw.onWsClose(): ${close_msg}${reason}`)

		window.clearInterval(this.heartbeatIntervalId)
	}

	onMsgDispatch(data)
	{
		let d = data.d

		console.log(`Gw.onMsgDispatch(): t = ${data.t}`)

		this.lastSeq = data.s

		switch (data.t)
		{
			case 'READY': this.onReady(d); break
			case 'GUILD_CREATE': this.onGuildCreate(d); break
			case 'MESSAGE_CREATE': this.onMessageCreate(d); break
			case 'VOICE_SERVER_UPDATE': this.onVoiceServerUpdate(d); break
			case 'VOICE_STATE_UPDATE': this.onVoiceStateUpdate(d); break
			default: 
				console.log(`Gw.onMsgDispatch(): Dispatch message not handled`)
				break
		}
	}

	onMsgReconnect(data)
	{
		console.log('Gw.onMsgReconnect()')

		this.sendResume()
	}

	onMsgInvalSession(data)
	{
		let delay = 5000

		console.log('Gw.onMsgInvalSession()')

		window.setTimeout(() => {
			console.log(`Gw.onMsgInvalSession(): Attempting to identify again in ${delay} ms...`)
			window.setTimeout(() => this.sendIdent(), delay)
		}, 0)
	}

	onMsgHello(data)
	{
		let d = data.d

		console.log('Gw.onMsgHello()')
		this.heartbeatInterval = d.heartbeat_interval

		if (this.heartbeatIntervalId !== null)
			window.clearInterval(this.heartbeatIntervalId)

		this.heartbeatIntervalId = window.setInterval(() => {
			this.sendHeartbeat()
		}, this.heartbeatInterval)

		this.sendIdent()
	}

	onMsgHeartbeatAck(data)
	{
		console.log('Gw.onMsgHeartbeatAck()')
	}

	onReady(payload)
	{
		console.log('Gw.onReady(): ', payload)

		this.idUser = payload.user.id
		this.idSession = payload.session_id

		for (let guild of payload.guilds)
			this.onGuildCreate(guild)
	}

	onGuildCreate(guild)
	{
		console.log('Gw.onGuildCreate(): ', guild)

		if (guild.voice_states === undefined || guild.voice_states === null)
			guild.voice_states = []

		for (let state of guild.voice_states)
		{
			state.guild_id = guild.id
			this.onVoiceStateUpdate(state)
		}
	}

	onMessageCreate(msg)
	{
		console.log('Gw.onMessageCreate(): ', msg)
	}

	onVoiceServerUpdate(state)
	{
		console.log('Gw.onVoiceServerUpdate(): ', state)
	}

	onVoiceStateUpdate(state)
	{
		console.log('Gw.onVoiceStateUpdate(): ', state)
	}
}

class Rest
{
	static API_VER = 6
	static API_ROOT = 'https://discord.com/api'

	token

	constructor(token)
	{
		this.token = token
	}

	send(method, url, data, headers)
	{
		if (data === undefined)
			data = null
		if (headers === undefined)
			headers = null

		if (headers === null)
			headers = new Headers()

		headers.append('Authorization', `Bot ${this.token}`)

		let params = {
			method: method,
			headers: headers
		}

		if (data !== null)
			params.body = data

		let send = (url, params) => {
			return fetch(`${Rest.API_ROOT}/v${Rest.API_VER}${url}`, params).then(ans => {
				if (ans.status !== 429)
					return ans

				return ans.json().then(data => {
					return new Promise(resolve => {
						let delay = data.retry_after
						console.log(`Rest.send(): Rate limit reached: Delaying send after ${delay} ms...`)
						window.setTimeout(() => resolve(send(url, params)), delay)
					})
				})
			})
		}

		console.log(`Rest.send(): url = ${url}, params = ${JSON.stringify(params, 1)}`)
		send(url, params)
	}

	createEmbedMessage(id_chan, title, msg, fields, footer)
	{
		let params = {
			embed: {
				title: title,
				description: msg
			}
		}

		if (fields !== undefined
				&& fields !== null)
			params.embed.fields = fields

		if (footer !== undefined
				&& footer !== null)
		{
			params.embed.footer = {
				text: footer
			}
		}

		let data = JSON.stringify(params)

		return this.send('POST', `/channels/${id_chan}/messages`, data, new Headers({
			'Content-Type': 'application/json'
		}))
	}
}

class Bot
{
	token
	gw
	rest

	constructor(token)
	{
		this.token = token
		this.gw = null
		this.rest = null
	}

	boot()
	{
		if (this.gw === null)
			this.gw = new Gw(this.token)
		if (this.rest === null)
			this.rest = new Rest(this.token)

		this.gw.boot()
	}
}
